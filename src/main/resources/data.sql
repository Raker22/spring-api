insert into supplier (name) values
  ('Josh Raker'),
  ('Larry Baker'),
  ('Luis Garza');

insert into part (name) values
  ('Gear'),
  ('Screw');

insert into supplier_part (supplier_id, part_id, quantity) values
  (1, 1, 50),
  (1, 2, 100);