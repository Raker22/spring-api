package com.floor8.springapi

import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root
import javax.servlet.ServletRequest

class QuerySpecification<T>(private val params: Map<String, Array<out Any>>) : Specification<T> {
    constructor(request: ServletRequest): this(request.parameterMap)

    override fun toPredicate(root: Root<T>, query: CriteriaQuery<*>, cb: CriteriaBuilder): Predicate {
        val predicates: MutableList<Predicate> = mutableListOf()

        params.forEach {
            // get the property and operator from the key string
            val propOp: List<String> = it.key.split("__")
            val prop: String
            val op: SpecOp

            if (propOp.size == 1) {
                prop = it.key
                op = SpecOp.EQ
            }
            else {
                prop = propOp[0]
                op = SpecOp[propOp[1]]
            }

            // run the operator on each value
            predicates.add(cb.or(*it.value.map { op.operate(root, cb, prop, it) }.toTypedArray()))
        }

        return cb.and(*predicates.toTypedArray())
    }
}
