package com.floor8.springapi

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class SpringBoot

fun main(args: Array<String>) {
    SpringApplication.run(SpringBoot::class.java, *args)
}
