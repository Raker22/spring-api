package com.floor8.springapi

interface KeyModel: Model {
    // the entity's unique identifier
    var id: Long?
}
