package com.floor8.springapi

import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.Path
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

enum class SpecOp(val stringRep: String) {
    EQ("eq") {
        override fun <T> operate(root: Root<T>, builder: CriteriaBuilder, property: String, value: Any): Predicate {
            return builder.equal(getProperty<T, String>(root, property), value.toString())
        }
    },
    GT("gt") {
        override fun <T> operate(root: Root<T>, builder: CriteriaBuilder, property: String, value: Any): Predicate {
            return builder.greaterThan(getProperty(root, property), value.toString())
        }
    },
    GTE("gte") {
        override fun <T> operate(root: Root<T>, builder: CriteriaBuilder, property: String, value: Any): Predicate {
            return builder.greaterThanOrEqualTo(getProperty(root, property), value.toString())
        }
    },
    LT("lt") {
        override fun <T> operate(root: Root<T>, builder: CriteriaBuilder, property: String, value: Any): Predicate {
            return builder.lessThan(getProperty(root, property), value.toString())
        }
    },
    LTE("lte") {
        override fun <T> operate(root: Root<T>, builder: CriteriaBuilder, property: String, value: Any): Predicate {
            return builder.lessThanOrEqualTo(getProperty(root, property), value.toString())
        }
    },
    LIKE("like") {
        override fun <T> operate(root: Root<T>, builder: CriteriaBuilder, property: String, value: Any): Predicate {
            return builder.like(getProperty(root, property), value.toString())
        }
    };

    abstract fun <T> operate(root: Root<T>, builder: CriteriaBuilder, property: String, value: Any): Predicate

    companion object {
        private val map: Map<String, SpecOp> = SpecOp.values().associateBy {
            it.stringRep
        }

        operator fun get(op: String): SpecOp {
            return map[op] ?: throw OperatorNotFoundException("Operator \"$op\" not found")
        }

        fun <T, K> getProperty(root: Root<T>, property: String): Path<K> {
            val properties: MutableList<String> = property.split(".").toMutableList()
            val lastProp: String = properties.last()
            var path: Path<out Any?> = root

            properties.removeAt(properties.size - 1)

            properties.forEach {
                path = path.get(it)
            }

            return path.get<K>(lastProp)
        }
    }

    class OperatorNotFoundException(message: String): RuntimeException(message)
}
