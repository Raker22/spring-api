package com.floor8.springapi

interface Model {
    // returns true if the entity is valid and can be saved
    fun isValid(): Boolean
}
