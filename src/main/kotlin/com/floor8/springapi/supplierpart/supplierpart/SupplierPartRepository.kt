package com.floor8.springapi.supplierpart.supplierpart

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface SupplierPartRepository: JpaRepository<SupplierPart, Long>, QueryDslPredicateExecutor<SupplierPart>, JpaSpecificationExecutor<SupplierPart>
