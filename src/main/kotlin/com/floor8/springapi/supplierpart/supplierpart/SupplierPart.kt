package com.floor8.springapi.supplierpart.supplierpart

import com.fasterxml.jackson.annotation.JsonIgnore
import com.floor8.springapi.Model
import com.floor8.springapi.supplierpart.part.Part
import com.floor8.springapi.supplierpart.supplier.Supplier
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@IdClass(SupplierPart.CustomId::class)
class SupplierPart: Model {
    data class CustomId(
        var supplierId: Long? = null,
        var partId: Long? = null
    ): Serializable

    @Id
    @Column(name = "supplier_id")
    var supplierId: Long? = null

    @Id
    @Column(name = "part_id")
    var partId: Long? = null

    @MapsId("supplier_id")
    @ManyToOne(cascade = arrayOf(CascadeType.REFRESH))
    @JsonIgnore
    @JoinColumn(nullable = false)
    var supplier: Supplier? = null

    @MapsId("part_id")
    @ManyToOne(cascade = arrayOf(CascadeType.REFRESH))
    @JsonIgnore
    @JoinColumn(nullable = false)
    var part: Part? = null

    @Column(nullable = false)
    var quantity: Int = 0

    @JsonIgnore
    override fun isValid(): Boolean {
        return this.supplier != null &&
            this.part != null
    }

    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> {
                true
            }
            other is SupplierPart -> {
                this.supplierId == other.supplierId && this.partId == other.partId
            }
            else -> {
                false
            }
        }
    }

    override fun hashCode(): Int {
        return Objects.hash(this.supplierId, this.partId)
    }
}
