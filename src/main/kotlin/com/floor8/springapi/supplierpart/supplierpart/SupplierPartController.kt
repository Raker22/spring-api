package com.floor8.springapi.supplierpart.supplierpart

import com.floor8.springapi.QuerySpecification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("supplierparts/supplierParts")
class SupplierPartController {
    companion object {
        val supplierPart: QSupplierPart = QSupplierPart.supplierPart
    }

    @Autowired
    private lateinit var supplierPartRepo: SupplierPartRepository

    @GetMapping
    @ResponseBody
    fun getSuppliers(request: HttpServletRequest): Iterable<SupplierPart> {
        return supplierPartRepo.findAll(QuerySpecification<SupplierPart>(request))
    }
}
