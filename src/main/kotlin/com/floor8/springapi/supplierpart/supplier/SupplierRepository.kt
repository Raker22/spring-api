package com.floor8.springapi.supplierpart.supplier

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface SupplierRepository: JpaRepository<Supplier, Long>, QueryDslPredicateExecutor<Supplier>, JpaSpecificationExecutor<Supplier>
