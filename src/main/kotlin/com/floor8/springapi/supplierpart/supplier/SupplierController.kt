package com.floor8.springapi.supplierpart.supplier

import com.floor8.springapi.QuerySpecification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("supplierparts/suppliers")
class SupplierController {
    companion object {
        val supplier: QSupplier = QSupplier.supplier
    }

    @Autowired
    private lateinit var supplierRepo: SupplierRepository

    @GetMapping
    @ResponseBody
    fun getSuppliers(request: HttpServletRequest): Iterable<Supplier> {
        return supplierRepo.findAll(QuerySpecification<Supplier>(request))
    }
}
