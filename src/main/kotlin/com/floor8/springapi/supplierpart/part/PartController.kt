package com.floor8.springapi.supplierpart.part

import com.floor8.springapi.QuerySpecification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("supplierparts/parts")
class PartController {
    companion object {
        val part: QPart = QPart.part
    }

    @Autowired
    private lateinit var partRepo: PartRepository

    @GetMapping
    @ResponseBody
    fun getSuppliers(request: HttpServletRequest): Iterable<Part> {
        return partRepo.findAll(QuerySpecification<Part>(request))
    }
}
