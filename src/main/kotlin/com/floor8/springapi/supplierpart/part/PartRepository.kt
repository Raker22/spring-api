package com.floor8.springapi.supplierpart.part

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.querydsl.QueryDslPredicateExecutor

interface PartRepository: JpaRepository<Part, Long>, QueryDslPredicateExecutor<Part>, JpaSpecificationExecutor<Part>
