#!/usr/bin/env bash

sleep_time=5
current=0
timeout=90

# Attempt to start rest service until successful or timeout
until [ ${current} -gt ${timeout} ]; do
    java -jar /service.jar && break

    echo "Retrying in 5 seconds..."
    current=$[$current+$sleep_time]

    if [ ${current} -le ${timeout} ]; then
        sleep ${sleep_time}
    else
        echo "Failed to start service after $timeout seconds"
    fi
done
