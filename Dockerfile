FROM java:8
VOLUME /tmp
ADD build/libs/service.jar service.jar
ADD service-entrypoint.sh service-entrypoint.sh
EXPOSE 8080
RUN bash -c 'touch /service.jar'
RUN bash -c 'touch /entrypoint.sh'
ENTRYPOINT bash /service-entrypoint.sh
